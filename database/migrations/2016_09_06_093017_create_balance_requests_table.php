<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('character_id')->unsigned();
            $table->decimal('amount', 19, 4);
            $table->text('reasoning');
            $table->enum('status', ['approved', 'rejected', 'pending'])->default('pending');
            $table->text('decision_reasoning')->nullable();
            $table->integer('reviewed_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('character_id')->references('id')->on('characters');
            $table->foreign('reviewed_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_requests');
    }
}
