# Galactic Bank
Description to be provided.

## Installation Steps
The easiest method of devleopment for the project would be through using Homestead. You can find installation steps for that at https://laravel.com/docs/5.3/homestead

Once you have your local environment set up you want to run the following commands:
```
$ git clone https://{username}@bitbucket.org/swrp/galactic-bank.git
$ cd galactic-bank
$ composer install
$ php artisan migrate
$ php artisan db:seed
```
