<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/*----------------------------------------------------------------/*
 * Character Routes
/*----------------------------------------------------------------*/
Route::group(['prefix' => 'character'], function() {
    Route::get('/',             ['uses' => 'CharacterController@index',      'as' => 'character-list']);
    Route::get('/create',       ['uses' => 'CharacterController@create',     'as' => 'character-create']);
    Route::post('/create',      ['uses' => 'CharacterController@save',       'as' => 'character-create-save']);
    Route::get('/{id}',         ['uses' => 'CharacterController@show',       'as' => 'character-profile']);
    Route::delete('/{id}',      ['uses' => 'CharacterController@destroy',    'as' => 'character-delete']);
    Route::get('/{id}/update',  ['uses' => 'CharacterController@update',     'as' => 'character-update']);
    Route::post('/{id}/update', ['uses' => 'CharacterController@updateSave', 'as' => 'character-update-save']);
});

/*----------------------------------------------------------------/*
 * Transaction Routes
/*----------------------------------------------------------------*/
Route::group(['prefix' => 'transaction'], function() {
    Route::get('/',        ['uses' => 'TransactionController@index', 'as' => 'transaction-list']);
    Route::get('/create',  ['uses' => 'TransactionController@new',   'as' => 'transaction-create']);
    Route::post('/create', ['uses' => 'TransactionController@send',  'as' => 'transaction-send']);
    Route::get('/{id}',    ['uses' => 'TransactionController@show',  'as' => 'transaction-list-show']);
});

/*----------------------------------------------------------------/*
 * Moderator Control Panel Routes
/*----------------------------------------------------------------*/
Route::group(['prefix' => 'mcp', 'middleware' => 'moderator'], function() {
    Route::get('/',                   ['uses' => 'ModeratorControlPanel@index',         'as' => 'moderator-dashboard']);
    Route::get('/request-list',       ['uses' => 'ModeratorControlPanel@requestList',   'as' => 'request-list']);
    Route::get('/request-list/{id}',  ['uses' => 'ModeratorControlPanel@requestShow',   'as' => 'request-show']);
    Route::post('/request-list/{id}', ['uses' => 'ModeratorControlPanel@requestReview', 'as' => 'request-review']);
});

/*----------------------------------------------------------------/*
 * Leaderboard Routes
/*----------------------------------------------------------------*/
Route::group(['prefix' => 'leaderboard'], function() {
    Route::get('/', ['uses' => 'LeaderboardsController@index', 'as' => 'leaderboard']);
});