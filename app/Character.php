<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Character extends Model
{
    use SoftDeletes; 

    public static function nameExists($name)
    {
        $character = Character::where('name', $name)->first();
        if ($character) {
            return true;
        }

        return false;
    }
}
