<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Character;
use App\BalanceRequest;
use App\Balance;

class CharacterController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the Character Dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $characters = Character::where('characters.user_id', \Auth::user()->id)
                            ->join('balance_requests', 'balance_requests.character_id', '=', 'characters.id')
                            ->leftJoin('balances', 'balances.character_id', 'characters.id')
                            ->select('characters.*', 'characters.id as character_id', 'balance_requests.*', 'balances.amount as balance')
                            ->get();

        return view('character.index')->withCharacters($characters);
    }

    /**
     * Show the Character Profile page
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $character = Character::where('characters.id', $id)
                            ->join('balance_requests', 'balance_requests.character_id', '=', 'characters.id')
                            ->join('users', 'users.id', 'characters.user_id')
                            ->leftJoin('balances', 'balances.character_id', 'characters.id')
                            ->select('characters.*', 'characters.id as character_id', 'balance_requests.*', 'balances.amount as balance', 'users.name as username', 'users.id as user_id')
                            ->firstOrFail();


        return view('character.show', [
            'character' => $character
        ]);
    }

    /**
     * Show the Character Creation Screen
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('character.create');
    }

    public function destroy(Request $request, $id)
    {
        // Find the character.
        $character      = Character::find($id);  
        if ($character) {
            $character->delete();
        }

        return redirect('/character')->with('message', 'Character deleted.');
    }

    /**
     * Validates and saves an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'           => 'required|max:255',
            'faction'        => 'required',
            'balance'        => 'required|numeric|min:0',
            'balance_reason' => 'required'
        ]);

        if (Character::nameExists($request->name)) {
            return back()->with('error', 'That name is already in use.');
        }

        \DB::beginTransaction();

        $character          = new Character();
        $character->name    = $request->name;
        $character->faction = $request->faction;
        $character->user_id = \Auth::user()->id;
        $character->save();
        
        $balanceRequest               = new BalanceRequest();
        $balanceRequest->character_id = $character->id;
        $balanceRequest->amount       = $request->balance;
        $balanceRequest->reasoning    = $request->balance_reason;
        $balanceRequest->save();

        if ($balanceRequest->id > 0) {
            \DB::commit();    
            return back()->with('message', 'Character successfully created');
        }

        \DB::rollBack();
        return back()->with('error', 'Something seems to have gone wrong, please try again.');
    }

    public function update($id) 
    {
        $character = Character::where('characters.id', $id)
                            ->join('balance_requests', 'balance_requests.character_id', '=', 'characters.id')
                            ->join('users', 'users.id', 'characters.user_id')
                            ->leftJoin('balances', 'balances.character_id', 'characters.id')
                            ->select('characters.*', 'characters.id as character_id', 'balance_requests.*', 'balances.amount as balance', 'users.name as username', 'users.id as user_id')
                            ->firstOrFail();

        return view('character.update', [
            'character' => $character
        ]);
    }

    public function updateSave(Request $request, $id)
    {
        $this->validate($request, [
            'name'           => 'max:255',
            'balance'        => 'numeric|min:0',
        ]);

        $character = Character::findOrFail($id);
        if ($request->name !== $character->name) {
            if (Character::nameExists($request->name) === false) {
                $character->name = $request->name; 
            } 
        }

        if (!empty($request->faction))       { $character->faction = $request->faction; }
        if (!empty($request->description))   { $character->description = trim($request->description); }
        
        $character->save();

        if (!$request->balance) {
            return back()->with('message', 'Character details updated.');
        }

        // Lets check their balance request.
        $balanceRequest = BalanceRequest::where('character_id', $id)->first();

        if ($balanceRequest->status !== 'rejected') {
            return back()->with('error', 'That character\'s balance has not been rejected.');
        }

        $balanceRequest->amount    = $request->balance;
        $balanceRequest->reasoning = $request->balance_reason;
        $balanceRequest->status    = 'pending';
        $balanceRequest->save();
    
        return back()->with('message', 'Character details updated.');
    }
}
