<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Character;
use App\Transaction;
use App\Balance;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the transaction character list.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $characters = Character::where('characters.user_id', \Auth::user()->id)->get();
        return view('transaction.index', ['characters' => $characters]);
    }

    /**
     * Show the specific transactions for a character.
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $character = Character::where('id', $id)->firstOrFail();
        $sent      = Transaction::where('sender_id', $character->id)
                            ->join('characters', 'characters.id', 'transactions.recipient_id')
                            ->select('characters.name as name', 'characters.deleted_at', 'transactions.*')
                            ->get();

        $recieved  = Transaction::where('recipient_id', $character->id)
                            ->join('characters', 'characters.id', 'transactions.sender_id')
                            ->select('characters.name as name', 'characters.deleted_at', 'transactions.*')
                            ->get();

        return view('transaction.show', [
            'character' => $character,
            'sent'      => $sent,
            'recieved'  => $recieved
        ]);
    }

    /**
     * Show the specific transactions for a character.
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        $characters = Character::where('characters.user_id', \Auth::user()->id)
                            ->join('balances', 'characters.id', 'balances.character_id')
                            ->select('characters.id AS character_id', 'characters.*', 'balances.*')
                            ->get();

        return view('transaction.new', [
            'characters' => $characters
        ]);
    }

    public function send(Request $request)
    {
        $recipient = Character::where('characters.name', $request->recipient)
                            ->join('balances', 'balances.character_id', 'characters.id')
                            ->select('characters.id as character_id', 'balances.amount as balance', 'characters.*', 'balances.*')
                            ->first();

        if (!$recipient) {
            return back()->with('error', 'We could not find that character\'s balance. Please ensure you typed the name correct.');
        }

        if (!isset($recipient->amount)) {
            return back()->with('error', 'That recipient does not have an active balance, please wait for them to be approved.');
        }


        $sender = Character::where('characters.id', $request->character_id)
                            ->join('balances', 'balances.character_id', 'characters.id')
                            ->select('characters.id as character_id', 'balances.amount as balance', 'characters.*', 'balances.*')
                            ->first();

        if ($sender->character_id === $recipient->character_id) {
            return back()->with('error', 'You cannot perform a transaction with yourself, get some friends please.');
        }

        if ($sender->user_id !== \Auth::user()->id) {
            return back()->with('error', 'Well that was sneaky now, wasn\'t it.');
        }

        if ($request->amount > $sender->balance) {
            return back()->with('error', 'You do not have enough funds to process that request.');
        }

        if (!$request->amount > 0) {
            return back()->with('error', 'You must send more than 0 credits for a valid transaction.');
        }

        // Perform the transaction.
        \DB::beginTransaction();

        $senderBalance            = Balance::where('character_id', $sender->character_id)->first();
        $senderBalance->amount    = $senderBalance->amount - $request->amount;
        $senderBalance->save();

        $recipientBalance         = Balance::where('character_id', $recipient->character_id)->first();
        $recipientBalance->amount = $recipientBalance->amount + $request->amount;
        $recipientBalance->save();

        $transaction               = new Transaction();
        $transaction->sender_id    = $sender->character_id;
        $transaction->recipient_id = $recipient->character_id;
        $transaction->amount       = $request->amount;

        if (isset($request->note)) {
            $transaction->note = $request->note;
        }
        
        $transaction->save();

        if ($transaction->id > 0) {
            \DB::commit();    
            return back()->with('message', 'Transaction successfully created');
        }

        \DB::rollBack();
        return back()->with('error', 'Something seems to have gone wrong, please try again.');

    }
}
