<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BalanceRequest;
use App\Balance;
use App\Character;

class ModeratorControlPanel extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the Moderator Control Panel Index.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = BalanceRequest::where('status', 'pending')->get();

        return view('mcp.index', [
            'requests' => $requests
        ]);
    }

    /**
     * Show the Balance Request List.
     * @return \Illuminate\Http\Response
     */
    public function requestList()
    {
        $requests = BalanceRequest::where('balance_requests.status', 'pending')
                                    ->join('characters', 'characters.id', '=', 'balance_requests.character_id')
                                    ->join('users', 'characters.user_id', '=', 'users.id')
                                    ->select('balance_requests.*', 'characters.name', 'users.name AS username')
                                    ->get();

        $completed = BalanceRequest::where('balance_requests.status', '!=', 'pending')
                                    ->join('characters', 'characters.id', '=', 'balance_requests.character_id')
                                    ->join('users', 'characters.user_id', '=', 'users.id')
                                    ->select('balance_requests.*', 'characters.name', 'users.name AS username')
                                    ->paginate(5);


        return view('mcp.request_list', [
            'requests'  => $requests,
            'completed' => $completed
        ]);
    }

    /**
     * Show a specific Balance Request.
     * @return \Illuminate\Http\Response
     */
    public function requestShow($id)
    {
        $request = BalanceRequest::where('balance_requests.id', $id)
                                    ->join('characters', 'characters.id', '=', 'balance_requests.character_id')
                                    ->join('users', 'characters.user_id', '=', 'users.id')
                                    ->select('balance_requests.*', 'characters.name', 'users.name AS username')
                                    ->first();

        
        $reviewer = 'Unknown';
        if ($request->status !== 'pending') {
            $reviewer = BalanceRequest::where('balance_requests.id', $id)
                                        ->join('users', 'users.id', 'balance_requests.reviewed_by')
                                        ->select('users.name')
                                        ->first()->name;
        }


        return view('mcp.request_show', [
            'request'  => $request,
            'reviewer' => $reviewer
        ]);
    }

    public function requestReview(Request $request, $id) 
    {
        $this->validate($request, [
            'decision'        => 'required',
            'decision_reason' => 'required',
        ]);

        $balanceRequest = BalanceRequest::findOrFail($id);

        // Don't allow already approved balances to be overwritten.
        if ($balanceRequest->status !== 'pending') {
            return redirect('/mcp/request-list')->with('error', 'That account has already been reviewed.');
        }

        // Don't allow users to review their own balances.
        $requestedOwner = Character::where('characters.id', $balanceRequest->character_id)
                    ->join('users', 'users.id', 'characters.user_id')
                    ->select('users.id')
                    ->first();


        if ($requestedOwner->id === \Auth::user()->id && \Auth::user()->permission_level !== 3) {
            return redirect('/mcp/request-list')->with('error', 'You are not allowed to review your own characters.');
        }



        $balanceRequest->decision_reasoning = $request->decision_reason;
        $balanceRequest->status = ($request->decision === 'approve') ? 'approved' : 'rejected';
        $balanceRequest->reviewed_by = \Auth::user()->id;
        
        if ($balanceRequest->save()) {

            if ($request->decision === 'approve') {
                $balance               = new Balance();
                $balance->character_id = $balanceRequest->character_id;
                $balance->amount       = $balanceRequest->amount;
                $balance->save();
            }
            
            return redirect('/mcp/request-list')->with('message', 'Review Completed.');
        }

        return back()->with('error', 'There was an issue saving your review, please try again.');


    }
}
