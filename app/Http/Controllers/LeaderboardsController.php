<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Balance;

class LeaderboardsController extends Controller
{
     /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $balances = Balance::orderBy('balances.amount', 'desc')
                            ->join('characters', 'balances.character_id', 'characters.id')
                            ->join('users', 'characters.user_id', 'users.id')
                            ->select('balances.*', 'characters.name AS name', 'users.name as username')
                            ->get();

        return view('leaderboard.index', [
            'balances' => $balances
        ]);
    }
}
