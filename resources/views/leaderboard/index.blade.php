@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Leaderboards</div>

                <div class="panel-body">

                    @if ($balances->count())
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Client Name</td>
                                <td>Character Name</td>
                                <td>Balance</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $rank = 1; ?>
                            @foreach ($balances as $balance)
                                <tr>
                                    <td>{{ $rank }}</td>
                                    <td>{{ $balance->username }}</td>
                                    <td>{{ $balance->name }}</td>
                                    <td><span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($balance->amount, 2) }}</td>
                                </tr>
                                <?php ++$rank ?>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
