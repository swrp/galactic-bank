@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Your Character</div>

                <div class="panel-body">

                    @if (session('message')) 
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif 

                    @if (session('error')) 
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif 
                    
                    <form class="form-horizontal {{ $errors->has('name') ? 'has-error' : '' }}" role="form" method="POST" action="{{ url('/character/create') }}">
                        {{ csrf_field() }}

                        <!-- Name -->
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Faction -->
                        <div class="form-group">
                            <label for="faction" class="col-md-4 control-label">Faction</label>
                            <div class="col-md-6">
                                <select class="form-control" name="faction" required>
                                    <option value="None">None</option>
                                    <option value="Jedi">Jedi</option>
                                    <option value="Sith">Sith</option>
                                    <option value="Other">Other</option>
                                </select>
                                @if ($errors->has('faction'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('faction') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Balance -->
                        <div class="form-group">
                            <label for="balance" class="col-md-4 control-label">Request Your Balance</label>
                            <div class="col-md-6">
                                <input id="balance" type="number" class="form-control" name="balance" value="{{ old('balance') }}" required>
                                @if ($errors->has('balance'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('balance') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Balance Reasoning -->
                        <div class="form-group">
                            <label for="balance_reason" class="col-md-4 control-label">Explain Your Request</label>
                            <div class="col-md-6">
                                <textarea id="balance_reason" class="form-control" name="balance_reason" required>
                                    {{ old('balance_reason') }}
                                </textarea>
                                @if ($errors->has('balance_reason'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('balance_reason') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Submit Form -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
