@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Character Profile</div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h1>{{ $character->name }}</h1>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 text-center">
                            <img src="https://assets.listia.com/assets/icons/default_avatar_m-6c1ffd7770da7ad49d965e6d40c4e2f1.png?s=100x100c&sig=7fd1a1586149929c" alt="Default Image" class="img-circle"/>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-5">
                                            Client Name
                                        </div>
                                        <div class="col-xs-12 col-sm-7">
                                            {{ $character->username }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-5">
                                            Balance
                                        </div>
                                        <div class="col-xs-12 col-sm-7">
                                            @if ($character->balance)
                                                <span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($character->balance, 2) }}
                                            @else
                                                <span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($character->amount, 2) }}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-5">
                                            Status
                                        </div>
                                        <div class="col-xs-12 col-sm-7">
                                            {{ ucwords($character->status) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="well">
                                @if (!empty($character->description))
                                    {{ $character->description }}
                                @else
                                    No description provided.
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12"><hr></div>
                    </div>
                    <div class="row text-center">
                        <div class="col-xs-12 col-sm-4">
                            
                        </div>
                        @if (\Auth::user()->id === $character->user_id)
                        <div class="col-xs-12 col-sm-4">
                            <a href="{{ url('/character/' . $character->character_id . '/update') }}"><button class="btn btn-primary">Update Character</button></a>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <form method="post" action="{{ url('/character/' . $character->character_id) }}">
                                <button class="btn btn-danger" type="submit" id="js-delete-character">Delete Character</button>
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        // Cache DOM
        var $deleteCharacter = $('#js-delete-character');

        $deleteCharacter.on('click', function(e) {
            var validate = confirm('This change is permanent, are you sure?')
            if (validate === false) {
                e.preventDefault();
            }
        });

    });
</script>
@endpush