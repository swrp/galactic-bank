@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Character List</div>
    
                <div class="panel-body">

                    @if (session('message'))   
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif

                    @if ($characters->count())
                        <div class="row">
                        @foreach ($characters as $character) 
                            <div class="col-sm-6 col-md-4">
                                <!-- Character -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ $character->name }}</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center center-block">
                                                <img src="https://assets.listia.com/assets/icons/default_avatar_m-6c1ffd7770da7ad49d965e6d40c4e2f1.png?s=100x100c&sig=7fd1a1586149929c" alt="Default Image" class="img-circle"/>
                                            </div>
                                            <div class="col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4">
                                                Balance
                                            </div>
                                            <div class="col-xs-12 col-sm-8">
                                                @if ($character->balance)
                                                    <span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($character->balance, 2) }}
                                                @else
                                                    <span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($character->amount, 2) }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4">
                                                Status
                                            </div>
                                            <div class="col-xs-12 col-sm-8">
                                                {{ ucwords($character->status) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <hr>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 text-center center-block">
                                                <a href="/character/{{ $character->character_id }}"><button class="btn btn-default">View More</button></a>
                                            </div>
                                        </div>
                                           
                                        

                                    </div>
                                </div>
                                  
                                
                              </div>
                        @endforeach
                        </div>
                    @else
                        You currently have no characters created, go <a href="{{ url('/character/create') }}">create one</a>.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
