@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Character Profile</div>

                <div class="panel-body">

                    @if (session('message')) 
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif

                    @if (session('error')) 
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

                    <form class="form-horizontal {{ $errors->has('name') ? 'has-error' : '' }}" role="form" method="POST" action="{{ url('/character/' . $character->character_id . '/update') }}">

                        {{ csrf_field() }}

                        <!-- Name -->
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $character->name }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                
                        <!-- Faction -->
                        <div class="form-group">
                            <label for="faction" class="col-md-4 control-label">Faction</label>
                            <div class="col-md-6">
                                <select class="form-control" name="faction">
                                    <option value="None">None</option>
                                    <option value="Jedi">Jedi</option>
                                    <option value="Sith">Sith</option>
                                    <option value="Other">Other</option>
                                </select>
                                @if ($errors->has('faction'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('faction') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    
                        <!-- Description -->
                        <div class="form-group">
                            <label for="description" class="col-md-4 control-label">Character Description</label>
                            <div class="col-md-6">
                                <textarea id="description" class="form-control" name="description">
                                    {{ $character->description }}
                                </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        @if ($character->status === 'rejected')
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <hr>
                                <h4>Update your Balance Request</h4>
                                <p>Your previous balance request was rejected, please see the feedback and update with a new balance.</p>
                            </div>

                            <!-- Balance -->
                            <div class="form-group">
                                <label for="balance" class="col-md-4 control-label">Request Your Balance</label>
                                <div class="col-md-6">
                                    <input id="balance" type="number" class="form-control" name="balance" value="{{ $character->amount }}" required>
                                    @if ($errors->has('balance'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('balance') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Balance Reasoning -->
                            <div class="form-group">
                                <label for="balance_reason" class="col-md-4 control-label">Explain Your Request</label>
                                <div class="col-md-6">
                                    <textarea id="balance_reason" class="form-control" name="balance_reason" required>
                                        {{ $character->reasoning }}
                                    </textarea>
                                    @if ($errors->has('balance_reason'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('balance_reason') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Feedback</div>
                                    <div class="panel-body">
                                        <p>{{ $character->decision_reasoning }}</p>
                                    </div>
                                </div>
                                <hr>
                            </div>


                        @endif

                         <!-- Submit Form -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Your Character
                                </button>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection