@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Balance Request</div>
                    <div class="panel-body">

                        <div class="row text-center">
                            <div class="col-xs-12">
                                <h3>{{ $request->username }}</h3>
                            </div>
                        </div>

                        <hr>

                        <div class="row text-center">
                            <div class="col-xs-12 col-sm-6">
                                <h4>Character Name</h4>
                                <p>{{ $request->name }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <h4>Balance Requested</h4>
                                <p><span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($request->amount, 2) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Reasoning</h4>
                                <p>{{ $request->reasoning }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                @if ($request->status === 'pending')
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Your Review
                        </div>
                        <div class="panel-body">

                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif

                            <form method="post">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="decision_reason" class="col-md-4 control-label text-right">Explain Your Review</label>
                                            <div class="col-md-6">
                                                <textarea id="decision_reason" class="form-control" name="decision_reason" required>
                                                    {{ old('decision_reason') }}
                                                </textarea>
                                                @if ($errors->has('decision_reason'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('decision_reason') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row text-center">
                                    <div class="col-sm-6">
                                        <button class="btn btn-success" type="submit" name="decision" value="approve">Approve</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn btn-danger" type="submit" name="decision" value="reject">Reject</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                @else
                    <div class="panel panel-default">
                        <div class="panel-heading">Balance Review</div>
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-6">
                                <h4>Reviewed By</h4>
                                <p>{{ $reviewer }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <h4>Decision</h4>
                                <p>{{ ucwords($request->status) }}</p>
                            </div>
                            <div class="col-xs-12">
                                <h4>Reason Given</h4>
                                <p>{{ $request->decision_reasoning }}</p>
                            </div>
                        </div>
                    </div>

                @endif
            </div>

        </div>
    </div>
</div>
@endsection
