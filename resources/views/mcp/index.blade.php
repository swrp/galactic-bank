@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">Moderator Control Panel</div>
                    <div class="panel-body">
                        @if ($requests->count())
                            There are currently <strong>{{ $requests->count() }}</strong> outstanding balance request(s).
                        @else
                            Nice job! There are currently no balance requests waiting for approval.
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
