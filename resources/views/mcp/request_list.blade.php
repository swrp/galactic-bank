@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Outstanding Balance Requests</div>
                    <div class="panel-body">
                        @if (session('message'))
                            <div class="alert alert-success">{{ session('message') }}</div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif

                        @if ($requests->count())
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Client Name</td>
                                        <td>Character Name</td>
                                        <td>Requested Amount</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests as $character)
                                    <tr>
                                        <td>{{ $character->username }}</td>
                                        <td>{{ $character->name }}</td>
                                        <td><span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($character->amount, 2) }}</td>
                                        <td><a href="{{ url('/mcp/request-list/' . $character->id) }}"><button class="btn btn-primary">Review</button></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            Nice job! There are currently no balance requests waiting for approval.
                        @endif
                    </div>
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading">Completed Balance Requests</div>
                    <div class="panel-body">
                        @if ($completed->count())
                            <ul class="list-group">
                            @foreach ($completed as $request)
                                <li class="list-group-item">
                                    <a href="/mcp/request-list/{{ $request->id }}">
                                        {{ $request->name }} 
                                        <span class="label label-{{ $request->status === 'approved' ? 'success' : 'danger' }} pull-right">{{ ucwords($request->status) }}</span>
                                    </a>
                                </li>
                            @endforeach
                            </ul>

                            {{ $completed->links() }}
                        @endif

                        
                        
                    </div>
                </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
