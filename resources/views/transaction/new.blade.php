@extends('layouts.app')

@section('content')
<div class="container">
     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new transactions</div>

                <div class="panel-body">
                    @if (session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    
                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

                    @if ($characters->count())
                    <form method="post">
                        {{ csrf_field() }}
                        <p>
                            Account to use
                            <select class="form-control" name="character_id">
                                @foreach ($characters as $character)
                                    <option value="{{ $character->character_id }}">{{ $character->name }} ({{ number_format($character->amount, 2) }})</option>
                                @endforeach
                            </select>
                        </p>

                        <p>Amount <input type="number" class="form-control" name="amount"></p>


                        <p>Recipient Name <input type="text" class="form-control" name="recipient"></p>

                        <p>Note <textarea class="form-control" name="note"></textarea>

                        <p><input type="submit" class="btn btn-primary pull-right" value="Send"></p>
                    </form>
                    @else
                        You don&apos;t have any characters to send a transaction yet.
                    @endif

                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
