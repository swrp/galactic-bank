@extends('layouts.app')

@section('content')
<div class="container">
     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Characters</div>

                <div class="panel-body">
                    @if ($characters->count())
                    <p>Select the character you wish to see your previous transactions for.</p>
                    <ul>
                        @foreach ($characters as $character)
                            <li><a href="{{ url('/transaction/' . $character->id )}}">{{ $character->name }}</a></li>
                        @endforeach
                    </ul>
                    @else 
                    <p>You have not created any characters first, go create one first.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
