@extends('layouts.app')

@section('content')
<div class="container">
     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Transactions for {{ $character->name }}</div>

                <div class="panel-body">
                    <h2>Sent Transactions</h2>
                    @if ($sent->count())
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Sent To</td>
                                <td>Amount Sent</td>
                                <td>Note <small><em>(Optional)</em></small></td>
                                <td>Date</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sent as $transaction)
                            <tr>
                                <td>
                                    @if (!is_null($transaction->deleted_at))
                                        {{ $transaction->name }} <small><em>(Inactive)</em></small>
                                    @else
                                        {{ $transaction->name }}
                                    @endif
                                </td>
                                <td><span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($transaction->amount, 2) }}</td>
                                <td>
                                    @if ($transaction->note)
                                        {{ $transaction->note }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>{{ (new \Carbon\Carbon($transaction->created_at))->format('d/m/Y H:i:s') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else 
                    <p>You have not sent any transactions with this character yet.</p>
                    @endif

                    <hr>

                    <h2>Recieved Transactions</h2>
                    @if ($recieved->count())
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Recieved From</td>
                                <td>Amount Recieved</td>
                                <td>Date</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($recieved as $transaction)
                            <tr>
                                <td>
                                    @if (!is_null($transaction->deleted_at))
                                        {{ $transaction->name }} <small><em>(Inactive)</em></small>
                                    @else
                                        {{ $transaction->name }}
                                    @endif
                                </td>
                                <td><span class="currency-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>{{ number_format($transaction->amount, 2) }}</td>
                                <td>{{ (new \Carbon\Carbon($transaction->created_at))->format('d/m/Y H:i:s') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else 
                    <p>You have not recieved any transactions with this character yet.</p>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
